# Stage for patching the headless variant of the Factorio installation.
FROM alpine AS factorio-headless-patcher
WORKDIR /opt

ARG FACTORIO_VERSION
ARG FACTORIO_USERNAME
ARG FACTORIO_TOKEN

# Create directory for the patched Factorio installation
RUN mkdir -p factorio-patched/bin/x64
# Download headless variant
RUN wget -O factorio-headless.tar.xz "https://www.factorio.com/get-download/${FACTORIO_VERSION}/headless/linux64?username=${FACTORIO_USERNAME}&token=${FACTORIO_TOKEN}"
# Extract headless variant
RUN tar -xJf factorio-headless.tar.xz
# Remove headless variant
RUN rm -rf factorio-headless.tar.xz
# Copy files from headless variant
RUN mv factorio/bin/x64/factorio factorio-patched/bin/x64/factorio
# Remove headless variant
RUN rm -rf factorio

# Stage for patching the full variant of the Factorio installation.
FROM alpine AS factorio-full-patcher
WORKDIR /opt

ARG FACTORIO_VERSION
ARG FACTORIO_USERNAME
ARG FACTORIO_TOKEN

# Create directory for the patched Factorio installation
RUN mkdir -p factorio-patched
# Download full variant
RUN wget -O factorio-full.tar.xz "https://www.factorio.com/get-download/${FACTORIO_VERSION}/expansion/linux64?username=${FACTORIO_USERNAME}&token=${FACTORIO_TOKEN}"
# Extract headless variant
RUN tar -xJf factorio-full.tar.xz
# Remove full variant
RUN rm -rf factorio-full.tar.xz
# Remove not needed file to reduce installation size
RUN find factorio \( \
            -path "factorio/bin/*" \
        -or -path "factorio/data/*/campaigns/*" \
        -or -path "factorio/data/*/fonts/*" \
        -or -path "factorio/data/*/graphics/achievement/*.png" \
        -or -path "factorio/data/*/graphics/decorative/*.png" \
        -or -path "factorio/data/*/graphics/entity/*.png" \
        -or -path "factorio/data/*/graphics/lut/*.png" \
        -or -path "factorio/data/*/graphics/particle/*.png" \
        -or -path "factorio/data/*/graphics/procession/*.png" \
        -or -path "factorio/data/*/graphics/space/*.png" \
        -or -path "factorio/data/*/graphics/sticker/*.png" \
        -or -path "factorio/data/*/graphics/terrain/*.png" \
        -or -path "factorio/data/*/menu-simulations/*.zip" \
        -or -path "factorio/data/*/scenarios/*" \
        -or -path "factorio/data/*/sound/*.ogg" \
        -or -path "factorio/data/changelog*.txt" \
        -or -path "factorio/doc-html/*" \
    \) -type f -delete
# Copy remaining files to the patched installation.
RUN mv factorio/* factorio-patched

# Final stage to be used by the exporter.
FROM alpine

COPY --from=factorio-headless-patcher /opt/factorio-patched /opt/factorio
COPY --from=factorio-full-patcher /opt/factorio-patched /opt/factorio
