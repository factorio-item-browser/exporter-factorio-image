# Factorio Item Browser - Exporter Factorio Image

This repository contains the Docker image containing a version of Factorio tailored to be used with the exporter.

## Building the image

When building the image, three arguments must be provided:

| Name              | Explanation                                                                                                        |
|-------------------|--------------------------------------------------------------------------------------------------------------------|
| FACTORIO_VERSION  | The version of Factorio to download and use. Make sure that this version actually exists.                          |
| FACTORIO_USERNAME | The username of the Factorio account used for downloading the game. The account must have access to the full game. |
| FACTORIO_TOKEN    | The API token of the Factorio account used for downloading the game.                                               |

```bash
docker build \
  --build-arg FACTORIO_VERSION=2.0.0 \
  --build-arg FACTORIO_USERNAME=fancy-account \
  --build-arg FACTORIO_TOKEN=fancy-token \
  .
```

## Factorio Instance tailored to the Exporter

The Factorio instance provided in the image is tailored to the exporter, and actually a combination of both, the 
headless variant and the full variant of the game.

The final image will have the Factorio instance placed in `/opt/factorio`.

### Headless variant

From the headless variant, only the executable file is taken.

The reason for picking the executable from the headless variant is, that the exporter only needs to create a savegame,
with which all needed scripts of the dumper mod is executed to get the data of the loaded mods. Running Factorio in 
headless mode means no graphics etc. gets initialized, removing the need to actually have according drivers installed
on the system.

### Full variant

We are taking most parts from the expansion variant of the game. We are actually most interested in the graphics,
especially the icons of all the items and recipes, which is why we combine the two variants in the first place.

To keep the image size small, some of the paths are removed from the game, as they are non-essential to run it and are
of no interest to the exporter. The following paths are removed:

| Path                                          | Reason                                                                               |
|-----------------------------------------------|--------------------------------------------------------------------------------------|
| `factorio/bin/*`                              | We do not need the executable file, as we take it from the headless variant.         |
| `factorio/data/*/campaigns/*`                 | We are not interested in the campaigns.                                              |
| `factorio/data/*/fonts/*`                     | We do not need the quite large fonts in any way.                                     |
| `factorio/data/*/graphics/achievements/*.png` | We are not interested in the achievements.                                           |
| `factorio/data/*/graphics/decorative/*.png`   | We do not need the decorative graphics as they are not involved in items or recipes. |
| `factorio/data/*/graphics/entity/*.png`       | We do not need the graphics of all the entities. We only need their icons.           |
| `factorio/data/*/graphics/lut/*.png`          | We do not need the LUTs.                                                             |
| `factorio/data/*/graphics/particle/*.png`     | We do not need any particle effect graphics.                                         |
| `factorio/data/*/graphics/procession/*.png`   | We do not need the procession thingies.                                              |
| `factorio/data/*/graphics/space/*.png`        | Wo do not need to render space.                                                      |
| `factorio/data/*/graphics/sticker/*.png`      | Wo do not need to apply any stickers to things.                                      |
| `factorio/data/*/graphics/terrain/*.png`      | We do not need any terrains.                                                         |
| `factorio/data/*/menu-simulations/*.zip`      | We do not need the menu simulations.                                                 |
| `factorio/data/*/scenarios/*`                 | We are not interested in the scenarios.                                              |
| `factorio/data/*/sound/*.ogg`                 | We do not need any sound files. Headless runs fine without them.                     |
| `factorio/data/changelog*.txt`                | We do not need the - quite large - changelog files.                                  |
| `factorio/doc-html/*`                         | We do not need the documentation of the script API.                                  |